const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');
const { stream } = require('browser-sync');

//compile scss into css
function style() {
    return gulp.src('css/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
           baseDir: "./",
           index: "/index.html"
        }
    });
    gulp.watch('css/*.scss', style)
    gulp.watch('./*.html').on('change',browserSync.reload);
    gulp.watch('./js/*.js').on('change', browserSync.reload);
}

function clean(){
    return del(['dist']);
}

function imgMin(){
    return gulp.src('./images/*.{jpg,png}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('dist/images'));
}

function useMin(){
    return gulp.src('./*html')
        .pipe(flatmap(function(stream, file){
            return stream.pipe(
                usemin({
                    css: [rev()],
                    html: [function(){return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                })
            );
        }))
        .pipe(gulp.dest('dist/'));
}

function copyFonts(){
    return gulp.src('./node_modules/open_iconic/font/fonts/*.{eot,otf,svg,ttf,woff}*')
    .pipe(gulp.dest('dist/fonts'));
}


exports.default =  gulp.series(clean, imgMin, useMin, copyFonts, style, watch);

//exports.default = gulp.series(clean, watch, style, imgMin, useMin);


exports.style = style;
exports.watch = watch;
//exports.default = build;



